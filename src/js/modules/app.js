(function($){

    var num = $('.preloader__cent-num');
    $('body').addClass('preload');

    if(window.scrollY > 800){
        $('.header').attr('data-wow-delay', '0.3s');
        $('.circle').attr('data-wow-delay', '1.4s');
        $('.facade__cont').attr('data-wow-delay', '2.4s');
    }

    var $window = $(window),
        lastScrollTop = 0,
        header = $('.header');

    function onScroll (e) {
        var top = $window.scrollTop();


        if(top > 103){
            header.addClass('show-header')
        }else{
            header.removeClass('show-header')
        }

        if (lastScrollTop > top) {
            header.addClass('scr-top');
        } else if (lastScrollTop < top) {

            header.removeClass('scr-top');
        }
        lastScrollTop = top;
    }

    $window.on('scroll', onScroll);

    /*preloader*/
    var interval = setInterval(function () {

        var current = parseInt(num.text());
        if(current < 99){

            num.text(current + 1);
            // var width = (current + 1) + '%';
            // $('.preloader__cent').css('left', width);
            // $('.preloader__line-inner-self-wrap').width(width);

        }else{
            $('.preloader__cent').remove();
        }

    }, 50);

    setTimeout(function () {
        $('.preloader').fadeOut()

        $('body').removeClass('addpreload');
        $('body').removeClass('preload');

        clearInterval(interval);


    }, 5200);

    $(function () {

        $('.tabs__btn').on('click', function () {
            if(!$(this).hasClass('active')){

                var index = $(this).index();
                $('.tabs__btn.active').removeClass('active');
                $(this).addClass('active');

                $('.tabs__btns-select-inner').text($(this).text());

                if($(window).width() < 768){
                    $('.tabs__btns-select').toggleClass('open');
                    $('.tabs__btns-inner').slideUp();
                }

                var activeItem = $('.tabs__item.active'),
                    newActive = $('.tabs__item').eq(index);

                activeItem.removeClass('move');
                activeItem.removeClass('active');
                newActive.addClass('active');

                setTimeout(function(){
                    newActive.addClass('move');
                },30);

            }

        });

        $('.tabs__btns-select').on('click', function () {
            $(this).toggleClass('open')
            $('.tabs__btns-inner').slideToggle();
        });

        $('[data-fancybox]').fancybox({
            youtube : {
                controls : 0,
                showinfo : 0
            }
        });

        function fixProductWidth(){
            var windowWidth = $(window).width(),
                contWidth = $('.container').width();
            if(windowWidth > 767){
                var addition = (windowWidth - contWidth) / 2;

                var basic = $('#basic');
                basic.width(contWidth + addition);
            }
        }

        //fixProductWidth();

        var options = {
            horizontal: 1,
            itemNav: 'basic',
            speed: 300,
            mouseDragging: 1,
            touchDragging: 1,
            scrollBar: '#scrollbar',
            scrollBy: 1,
            dragHandle: 1,
            dynamicHandle: 1,
            //clickBar: 1,
            prevPage: '.prev',
            nextPage: '.next',
        };

        var optionsTabs = {
            horizontal: 1,
            itemNav: 'basic',
            speed: 300,
            mouseDragging: 1,
            touchDragging: 1,
            //scrollBar: '#scrollbar',
            scrollBy: 1,
            dragHandle: 1,
            dynamicHandle: 1,
        };

        $('#basic').sly(options);

        $('.tabs__cont .frame').each(function (i, el) {

            $(this).sly({
                horizontal: 1,
                itemNav: 'basic',
                speed: 300,
                mouseDragging: 1,
                touchDragging: 1,
                scrollBar: '#scrollbar_' + i,
                scrollBy: 1,
                dragHandle: 1,
                dynamicHandle: 1,
            });

        });

        $(window).on('resize', function() {
            $('#basic').sly(false);
             //fixProductWidth();
             $('#basic').sly(options);



            $('.tabs__cont .frame').each(function (i, el) {

                $(this).sly(false);

                $(this).sly({
                    horizontal: 1,
                    itemNav: 'basic',
                    speed: 300,
                    mouseDragging: 1,
                    touchDragging: 1,
                    scrollBar: '#scrollbar_' + i,
                    scrollBy: 1,
                    dragHandle: 1,
                    dynamicHandle: 1,
                });

            });

        });



        // var timer;
        // var anyTime = 200; // любое время
        // $(window).on('resize', () => {
        //     if (timer){
        //         clearTimeout(timer);
        //         timer = false;
        //     }
        //     else{
        //         timer = setTimeout(() =>{
        //
        //             $('#basic').sly(false);
        //             fixProductWidth();
        //             $('#basic').sly(options);
        //
        //         }, anyTime);
        //     }
        // })


        /*form*/
        var formCtrl = $('.form-ctrl');

        formCtrl.on('focus', function () {
            $(this).parent().parent().addClass('show-label focused');
        });

        formCtrl.on('blur', function () {
            var formGroup = $(this).parent().parent();
            formGroup.removeClass('focused');
            if(!$(this).val()){
                formGroup.removeClass('show-label');
            }
        });

        $('.form-box .form-group label').on('click', function () {
            $(this).next().find('.form-ctrl').focus();
        });

        $('.order-form__radio label').on('click', function () {
            var index = $(this).data('index');

            $(this).closest('.order-form__radios').find('label.active').removeClass('active');
            $(this).addClass('active');

            $(this).closest('.order-form').find('.form-box.active').removeClass('active');
            $(this).closest('.order-form').find('.form-box').eq(index).addClass('active');


        });

        $('.form-ctrl-phone').mask('+38(000)0000000');

        // function checkRequire(field){
        //
        //     var par = $(field).parent().parent();
        //     var len = $(field).val().trim().length;
        //     if(!len){
        //         par.addClass('err-req');
        //         par.removeClass('err-format');
        //         return false;
        //     }else if(len && len < 3){
        //         par.addClass('err-format');
        //         par.removeClass('err-req');
        //         return false;
        //     }else{
        //         par.removeClass('err-format');
        //         par.removeClass('err-req');
        //         return true;
        //     }
        // }
        //
        // function checkEmail(field){
        //     var regex = /^[a-z0-9_\-\.]+@[a-z0-9_\-\.]+\.[a-z0-9]+$/gi;
        //
        //     var par = $(field).parent().parent();
        //     var len = $(field).val().trim().length;
        //     if(!len){
        //         par.addClass('err-req');
        //         par.removeClass('err-format');
        //         return false;
        //     }else if(len && !$(field).val().match(regex)){
        //         par.addClass('err-format');
        //         par.removeClass('err-req');
        //         return false;
        //     }else{
        //         par.removeClass('err-format');
        //         par.removeClass('err-req');
        //         return true;
        //     }
        // }
        //
        // function checkPhone(field){
        //
        //     var par = $(field).parent().parent();
        //     var len = $(field).val().trim().length;
        //
        //     if(!len){
        //         par.addClass('err-req');
        //         par.removeClass('err-format');
        //         return false;
        //     }else if(len && len < 14){
        //         par.addClass('err-format');
        //         par.removeClass('err-req');
        //         return false;
        //     }else{
        //         par.removeClass('err-format');
        //         par.removeClass('err-req');
        //         return true;
        //     }
        // }

        // $('.form-ctrl-name, .form-ctrl-position, .form-ctrl-company').on('blur', function () {
        //     checkRequire(this);
        // });
        //
        // $('.form-ctrl-name, .form-ctrl-position, .form-ctrl-company').on('change', function () {
        //     checkRequire(this);
        // });
        //
        // $('.form-ctrl-email').on('blur', function () {
        //     checkEmail(this);
        // });
        //
        // $('.form-ctrl-email').on('change', function () {
        //     checkEmail(this);
        // });
        //
        // $('.form-ctrl-phone').on('blur', function () {
        //     checkPhone(this);
        // });
        //
        // $('.form-ctrl-phone').on('change', function () {
        //     checkPhone(this);
        // });

        // $('.order-form').on('submit', function (e) {
        //     e.preventDefault();
        //     var name = $(this).find('input[name="name"]'),
        //         position = $(this).find('input[name="position"]'),
        //         email = $(this).find('input[name="email"]'),
        //         phone = $(this).find('input[name="phone"]'),
        //         company = $(this).find('input[name="company"]'),
        //         client = $(this).find('input[type="radio"]:checked');
        //
        //     if(checkRequire(name)
        //         && checkRequire(position)
        //         && checkRequire(company)
        //         && checkEmail(email)
        //         && checkPhone(phone) ){
        //
        //         var data = {
        //             name: name.val(),
        //             position: position.val(),
        //             email: email.val(),
        //             phone: phone.val(),
        //             company: company.val(),
        //             client: client.val()
        //         };
        //
        //
        //         $.ajax({
        //             url: $(this).attr('action'),
        //             method: "post",
        //             data
        //         }).done(res => {
        //
        //             $('.modal_overlay').fadeIn(400,
        //                 function(){
        //                     $('.modal_div')
        //                         .css('display', 'block')
        //                         .animate({opacity: 1, top: '50%'}, 200);
        //                 });
        //
        //         }).fail(err => {
        //             console.log(err);
        //             //alert('Сталася помилка');
        //         });
        //
        //     }
        //
        // });

        /*modal*/

        var overlay = $('.modal_overlay'),
            open_modal = $('.open_modal'),
            close = $('.modal_close, .modal_overlay'),
            modal = $('.modal_div');

        open_modal.on('click', function(e){
            e.preventDefault();
            $('body').addClass('modal_active');
            var div = $(this).attr('href');
            overlay.fadeIn(400,
                function(){
                    $(div)
                        .css('display', 'block')
                        .animate({opacity: 1, top: '50%'}, 200);
                });
        });

        close.on('click', function(){
            modal
                .animate({opacity: 0, top: '45%'}, 200,
                    function(){
                        $(this).css('display', 'none');
                        overlay.fadeOut(400);
                    }
                );
            $('body').removeClass('modal_active');
        });


        $('.toanchor').on('click', function(e) {
            e.preventDefault();
            var id  = $(this).attr('href'),
                top = $(id).offset().top;
            $('body,html').stop().animate({scrollTop: top}, 1500);
        });


        var wow = new WOW(
            {
                boxClass:     'wow',      // animated element css class (default is wow)
                animateClass: 'animated', // animation css class (default is animated)
                offset:       100,          // distance to the element when triggering the animation (default is 0)
                mobile:       true,       // trigger animations on mobile devices (default is true)
                live:         true,       // act on asynchronously loaded content (default is true)
                scrollContainer: null,    // optional scroll container selector, otherwise use window,
                resetAnimation: true,     // reset animation on end (default is true)
            }
        );
        wow.init();

        $('.open-close').on('click', function () {

            $('body').toggleClass('mob-menu-open');

        });

        $('.mobile-menu a').on('click', function () {

            $('body').removeClass('mob-menu-open');
            setTimeout(function() {

                $('.header').scrollTop(0);

            }, 100)
        });


    });

})(jQuery);

